import { Track } from "./Track";

interface NamedEntity {
  id: string;
  name: string;
}

export interface Album extends NamedEntity {
  images: AlbumImage[];
  artists?: Artist[];
  tracks?: PagingObject<Track>;
}

export interface Artist extends NamedEntity {
  popularity: number;
}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
  total: number;
}

// Response form server:
export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
