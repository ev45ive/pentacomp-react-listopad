export interface Playlist {
  id: number;
  name: string;
  favorite: boolean;
  color: string;
  /**
   * Dokumentacja Tracks
   */
  // tracks?: Array<Track>,
  tracks?: Track[]
}

interface Track {
  id: number;
  name: string;
}
