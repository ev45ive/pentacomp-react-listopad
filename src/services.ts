import { MusicSearch } from "./services/MusicSearch";
import { Security } from "./services/Security";

export const securityService = new Security();
securityService.getToken()

export const searchService = new MusicSearch(securityService);

