import { createStore, combineReducers, applyMiddleware } from "redux";
import { counter } from "./reducers/counter";
import { playlists, PlaylistsState } from "./reducers/playlists";
import { SearchState, search } from "./reducers/search";
import thunk from "redux-thunk";

export interface AppState {
  counter: number;
  playlists: PlaylistsState;
  search: SearchState;
}

const reducer = combineReducers<AppState>({
  counter,
  playlists,
  search
});

export const store = createStore(reducer, applyMiddleware(thunk));
