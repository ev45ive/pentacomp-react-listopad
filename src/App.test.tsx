import * as React from "react";
import * as ReactDOM from "react-dom";

interface Window {
  sessionStorage: any;
}
(window as Window).sessionStorage = {
  getItem(){},
  setItem(){},
  removeItem(){}
};

import App from "./App";

it("renders without crashing", () => {
  const div = document.createElement("div");
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
