import * as React from "react";
import * as ReactDOM from "react-dom";

import App from "./App";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

////
import { store } from "./store";
import { Provider } from "react-redux";

(window as any).store = store;

import { /* HashRouter */ BrowserRouter as Router } from "react-router-dom";

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
registerServiceWorker();

window["React"] = React;
window["ReactDOM"] = ReactDOM;
