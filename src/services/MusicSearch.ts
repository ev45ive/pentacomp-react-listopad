import axios, { AxiosError } from "axios";
import { Security } from "./Security";
import { AlbumsResponse } from "../models/Album";

export class MusicSearch {
  search_url = "https://api.spotify.com/v1/search";

  constructor(private security: Security) {}

  search(query = "batman") {
    if(!query){
      return Promise.resolve([])
    }

    //
    return axios
      .get<AlbumsResponse>(this.search_url, {
        headers: {
          Authorization: "Bearer " + this.security.getToken()
        },
        params: {
          q: query,
          type: "album"
        }
      })
      .then(resp => {
        return resp.data.albums.items;
      })
      .catch((err: AxiosError) => {
        const response = err.response;

        if (response !== undefined) {
          // Expired token
          if (response && response.status === 401) {
            this.security.authorize();
          }

          let error = response["data"].error;
          throw error;
        }
        throw err;
      });
  }
}
