// import axios from "axios";
export class Security {
  auth_url = "https://accounts.spotify.com/authorize";

  params = {
    client_id: "9a7a132160494936beb253b0d41ca9cd",
    response_type: "token",
    redirect_uri: "http://localhost:3000/"
  };

  authorize() {
    const { client_id, redirect_uri, response_type } = this.params;

    const params = `client_id=${client_id}&response_type=${response_type}&redirect_uri=${redirect_uri}`;

    sessionStorage.removeItem("token");
    location.replace(`${this.auth_url}?${params}`);
  }

  token = "";

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem("token") || "null");

    if (!this.token && location.hash) {
      const match = location.hash.match(/access_token=([^&]+)/);
      this.token = (match && match[1]) || "";
      location.hash = "";

      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}

// axios.interceptors.request.use( req => req.headers['Authorization'] = this.token )
