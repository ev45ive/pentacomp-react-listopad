import * as React from "react";
import "./App.css";
import { Route, Redirect, Switch, /* Link, */ NavLink } from "react-router-dom";
import { PlaylistsView } from "./playlists/PlaylistsView";
import { SearchView } from "./search/SearchView";
import { AlbumView } from "./search/AlbumView";

class App extends React.Component {
  public render() {
    return (
      <div>
        <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
          <div className="container">
            <NavLink className="navbar-brand" to="">
              MusicApp
            </NavLink>

            <div className="collapse navbar-collapse">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink className="nav-link" to="/search">
                    Search
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="nav-link" to="/playlists">
                    Playlists
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        <div className="container">
          <div className="row">
            <div className="col">
              <h3>MusicApp</h3>

              <Switch>
                <Route path="/search" component={SearchView} />
                <Route path="/search/:id" component={AlbumView} />
                <Route
                  path="/playlists"
                  component={PlaylistsView}
                  // render={(...args)=>{
                  //   debugger
                  //   return <PlaylistsView/>
                  // }}
                />
                <Redirect from="/" exact={true} to="/playlists" />
                <Redirect from="**" exact={true} to="/playlists" />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
