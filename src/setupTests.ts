// https://github.com/Microsoft/dts-gen
// npm i --save-dev jest-enzyme enzyme enzyme-adapter-react-16
// npm i --save-dev @types/enzyme @types/enzyme-adapter-react-16

import "jest-enzyme";
import { configure } from "enzyme";

import * as Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });



