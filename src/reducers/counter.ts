import { Action, Reducer, ActionCreator } from "redux";

interface INCREMENT extends Action<"INCREMENT"> {
  payload: number;
}

interface DECREMENT extends Action<"DECREMENT"> {
  payload: number;
}
type counterAction = INCREMENT | DECREMENT;

export const counter: Reducer<number> = (state = 0, action: counterAction) => {
  switch (action.type) {
    case "INCREMENT":
      return state + action.payload;
    case "DECREMENT":
      return state - action.payload;
    default:
      return state;
  }
};

/* Action Creators: */
export const increment: ActionCreator<INCREMENT> = (payload = 1) => ({
  type: "INCREMENT",
  payload
});

export const decrement: ActionCreator<DECREMENT> = (payload = 1) => ({
  type: "DECREMENT",
  payload
});

window["actions"] = {
  increment,
  decrement
};
