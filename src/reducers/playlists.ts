import { Playlist } from "src/models/Playlist";
import { Reducer, Action, ActionCreator } from "redux";

export interface PlaylistsState {
  list: Playlist[];
  selectedId: Playlist["id"] | null;
  query: string;
}

const initialState: PlaylistsState = {
  list: [
    {
      id: 123,
      name: "React Hits",
      favorite: false,
      color: "#ff0000"
    },
    {
      id: 234,
      name: "React TOP20",
      favorite: true,
      color: "#00ff00"
    },
    {
      id: 345,
      name: "Best of React",
      favorite: false,
      color: "#00ff00"
    }
  ],
  selectedId: null,
  query: ""
};

export const playlists: Reducer<PlaylistsState, playlistsActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_SELECT":
      return { ...state, selectedId: action.payload };

    case "PLAYLISTS_UPDATE":
      return {
        ...state,
        list: state.list.map(
          p => (p.id == action.payload.id ? action.payload : p)
        )
      };
    default:
      return state;
  }
};

/* Actions */
interface PLAYLISTS_SELECT extends Action<"PLAYLISTS_SELECT"> {
  payload: Playlist["id"];
}
interface PLAYLISTS_UPDATE extends Action<"PLAYLISTS_UPDATE"> {
  payload: Playlist;
}

type playlistsActions = PLAYLISTS_SELECT | PLAYLISTS_UPDATE;

export const selectPlaylist: ActionCreator<PLAYLISTS_SELECT> = (
  id: number
) => ({
  type: "PLAYLISTS_SELECT",
  payload: id
});

export const updatePlaylist: ActionCreator<PLAYLISTS_UPDATE> = payload => ({
  type: "PLAYLISTS_UPDATE",
  payload
});
