import { Reducer, Action, Dispatch } from "redux";
import { Album } from "../models/Album";
import { searchService } from "../services";

export interface SearchState {
  query: string;
  results: Album[];
  error: string;
  loading: boolean;
}

export const search: Reducer<SearchState> = (
  state = {
    error: "",
    results: [],
    query: "",
    loading: false
  },
  action: SearchActions
) => {
  switch (action.type) {
    case "SEARCH_STARTED":
      return {
        ...state,
        loading: true,
        query: action.payload.query
      };
    case "SEARCH_SUCCESS":
      return {
        ...state,
        loading: false,
        results: action.payload.results
      };
    case "SEARCH_ERROR":
      return {
        ...state,
        loading: false,
        error: action.payload.error
      };
    default:
      return state;
  }
};
type SearchActions = SEARCH_STARTED | SEARCH_SUCCESS | SEARCH_ERROR;

interface SEARCH_STARTED extends Action<"SEARCH_STARTED"> {
  payload: {
    query: string;
  };
}

interface SEARCH_SUCCESS extends Action<"SEARCH_SUCCESS"> {
  payload: {
    results: SearchState["results"];
  };
}

interface SEARCH_ERROR extends Action<"SEARCH_ERROR"> {
  payload: {
    error: string;
  };
}

export const searchAlbums = (query: string) => (dispatch: Dispatch) => {
  dispatch(<SEARCH_STARTED>{
    type: "SEARCH_STARTED",
    payload: { query }
  });

  searchService
    .search(query)
    .then(albums => {
      dispatch(<SEARCH_SUCCESS>{
        type: "SEARCH_SUCCESS",
        payload: {
          results: albums
        }
      });
    })
    .catch(error => {
      dispatch(<SEARCH_ERROR>{
        type: "SEARCH_ERROR",
        payload: {
          error: error.message
        }
      });
    });
};

// https://redux-saga.js.org/

// async () => {
//   try {
//     let albums = await searchService.search(query);
//     dispatch(<SEARCH_SUCCESS>{
//       type: "SEARCH_SUCCESS",
//       payload: {
//         results: albums
//       }
//     });
//   } catch (error) {
//     dispatch(<SEARCH_ERROR>{
//       type: "SEARCH_ERROR",
//       payload: {
//         error: error.message
//       }
//     });
//   }
// };
