import * as React from "react";
import { PlaylistsList } from "./containers/PlaylistsList";
import { PlaylistView } from "./containers/Playlist";
import { Route } from "react-router-dom";

export class PlaylistsView extends React.Component {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <Route path="/playlists/:id?" component={PlaylistsList} />
          </div>
          <div className="col">
            <Route path="/playlists/:id?" component={PlaylistView} />
          </div>
        </div>
      </div>
    );
  }
}
