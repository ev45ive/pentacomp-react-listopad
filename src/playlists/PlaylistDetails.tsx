import * as React from "react";
import { Playlist } from "src/models/Playlist";

interface IState {
  playlist: Playlist;
  editing: boolean;
}
interface IProps {
  playlist: Playlist;
  onSave(playlist: Playlist): void;
}

export class PlaylistDetails extends React.PureComponent<IProps, IState> {
  state: IState = {
    playlist: this.props.playlist,
    editing: false
  };

  constructor(props: IProps) {
    super(props);
    // this.state.playlist = props.playlist
  }

  static getDerivedStateFromProps(nextProps: IProps, state: IState) {
    return {
      playlist:
        nextProps.playlist.id == state.playlist.id
          ? state.playlist
          : nextProps.playlist
    };
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  // shouldComponentUpdate(nextProps: IProps, nextState: IState) {
  //   return (
  //     nextProps.playlist !== this.props.playlist
  //     || nextState !== this.state
  //   );
  // }

  // componentWillReceiveProps(nextProps: IProps) {
  //   this.setState({
  //     playlist: nextProps.playlist
  //   });
  // }

  edit = () => {
    this.setState((prevState, props) => ({
      editing: true,
      playlist: props.playlist
    }));
  };

  cancel = () => {
    this.setState({
      editing: false
    });
  };

  save = () => {
    this.props.onSave(this.state.playlist);
    this.setState({
      editing: false
    });
  };

  handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target,
      fieldName = target.name,
      fieldType = target.type,
      value = fieldType === "checkbox" ? target.checked : target.value;

    // const playlist = this.state.playlist;
    // playlist[fieldName] = value;

    this.setState(
      prevState =>
        prevState.playlist && {
          // playlist
          playlist: {
            ...prevState.playlist,
            [fieldName /* + 'placki' */]: value
          }
        }
    );
  };

  render() {
    const playlist = this.props.playlist;

    return (
      <div>
        {playlist && this.state.editing === false ? (
          <div>
            <h3>Playlist Details</h3>
            <dl>
              <dt>Name:</dt>
              <dd>{playlist.name}</dd>

              <dt>Favourite:</dt>
              <dd>{playlist.favorite ? "Yes" : "No"}</dd>

              <dt>Color</dt>
              <dd
                style={{
                  backgroundColor: playlist.color
                }}
              >
                {playlist.color}
              </dd>
            </dl>
            <input
              type="button"
              value="Edit"
              className="btn btn-info"
              onClick={this.edit}
            />
          </div>
        ) : (
          this.state.playlist && (
            <div>
              <h3>Form</h3>

              <div className="form-group">
                <label>Name:</label>
                <input
                  className="form-control"
                  type="text"
                  name="name"
                  value={this.state.playlist.name}
                  onChange={this.handleFieldChange}
                />
              </div>

              <div className="form-group">
                <label>Favourite</label>
                <input
                  type="checkbox"
                  name="favorite"
                  checked={this.state.playlist.favorite}
                  onChange={this.handleFieldChange}
                />
              </div>

              <div className="form-group">
                <label>Color</label>
                <input
                  type="color"
                  name="color"
                  value={this.state.playlist.color}
                  onChange={this.handleFieldChange}
                />
              </div>

              <input
                type="button"
                value="Cancel"
                className="btn btn-danger"
                onClick={this.cancel}
              />
              <input
                type="button"
                value="Save"
                className="btn btn-success"
                onClick={this.save}
              />
            </div>
          )
        )}
      </div>
    );
  }
}

// type Partial<T> = {
//   [k in keyof T]?: T[k]
// }
