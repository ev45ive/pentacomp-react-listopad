import { connect } from "react-redux";
import { Playlist } from "src/models/Playlist";
import { AppState } from "src/store";
import { updatePlaylist } from "../../reducers/playlists";
import { PlaylistDetails } from "../PlaylistDetails";
import * as React from "react";
import { RouteComponentProps } from "react-router";

interface StateProps {
  playlist: Playlist;
}

interface DispatchProps {
  onSave(playlist: Playlist): void;
}

type OwnProps = RouteComponentProps<{ id: string }>;

export const PlaylistView = connect<
  StateProps,
  DispatchProps,
  OwnProps,
  AppState
>(
  // Map State to Props
  (state: AppState, ownProps) => {
    const id = parseInt(ownProps.match.params.id, 10);

    const selected = state.playlists.list.find(p => p.id === id) || null;
    
    return {
      playlist: selected!
    };
  },

  // Map dispatch to Props
  dispatch => ({
    onSave(playlist: Playlist) {
      dispatch(updatePlaylist(playlist));
    }
  })
)(
  (props: StateProps & DispatchProps) =>
    props.playlist ? (
      <PlaylistDetails {...props} />
    ) : (
      <p>Please select playlist</p>
    )
);
