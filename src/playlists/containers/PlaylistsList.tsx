import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import { ItemsList } from "../ItemsList";
import { AppState } from "src/store";
import { Playlist } from "../../models/Playlist";
import { Dispatch } from "redux";
// import { selectPlaylist } from "src/reducers/playlists";
import { RouteComponentProps } from "react-router";

type StateProps = {
  playlists: Playlist[];
  selected: Playlist | null;
};
type OwnProps = RouteComponentProps<{ id: string }>;

const mapStateToProps: MapStateToProps<StateProps, OwnProps, AppState> = (
  state,
  ownProps
) => {
  const id = parseInt(ownProps.match.params.id, 10);

  const selected = state.playlists.list.find(p => p.id === id) || null;

  return {
    playlists: state.playlists.list,
    selected
  };
};

type DispatchProps = {
  onSelect(selected: Playlist): void;
};

const mapDispatchToProps: MapDispatchToProps<DispatchProps, OwnProps> = (
  dispatch: Dispatch,
  ownProps
) => {
  return {
    // onSelect: playlist => dispatch(selectPlaylist(playlist.id))

    onSelect(playlist: Playlist) {
      // replace() vs push()
      ownProps.history.replace({
        pathname: "/playlists/" + playlist.id
      });

      // albo zrob to w akcji!
    }
  };
};

const withPlaylists = connect(
  mapStateToProps,
  mapDispatchToProps
);

export const PlaylistsList = withPlaylists(ItemsList);
