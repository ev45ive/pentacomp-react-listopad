import * as React from "react";

interface IProps {
  onSearch(query: string): void;
}

export class SearchField extends React.Component<IProps> {
  componentDidMount() {
    if (this.inputRef.current) {
      this.inputRef.current.focus();
    }
  }

  // after setState, before Render
  getSnapshotBeforeUpdate(prevProps: IProps, prevState: {}) {
    return { placki: 123 };
  }

  // After Render / DOM UPDATE
  componentDidUpdate(prevProps: IProps, prevState: {}, snapshot: any) {
    console.log(snapshot);
  }

  inputRef = React.createRef<HTMLInputElement>();

  timeout: NodeJS.Timeout;

  queryChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    clearTimeout(this.timeout);

    const value = e.target.value;

    this.timeout = setTimeout(() => {
      this.props.onSearch(value);
    }, 400);
  };

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  render() {
    return (
      <div className="mb-3">
        <input
          ref={this.inputRef}
          type="text"
          className="form-control"
          onChange={this.queryChange}
        />
      </div>
    );
  }
}
