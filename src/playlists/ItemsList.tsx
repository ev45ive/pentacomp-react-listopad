import * as React from "react";
import { Playlist } from "../models/Playlist";

interface IProps {
  playlists: Playlist[];
  selected: Playlist | null;
  onSelect(selected: Playlist):void
}

export class ItemsList extends React.Component<IProps> {

  select = (selected: Playlist) => {
    this.props.onSelect(selected)
  };

  render() {
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map(playlist => {
            return (
              <div
                className={`list-group-item ${
                  playlist === this.props.selected ? "active" : ""
                }`}
                key={playlist.id}
                onClick={() => this.select(playlist)}
              >
                {playlist.name}
              </div>
            );
          })}
        </div>
      </div>
    );
  }

  // select(event:React.MouseEvent<HTMLDivElement>){
  //   event.persist()
  //   <div onClick={this.select} data-id={playlist.id}></div>
  //   console.log(event.target.dataset.id)
  // }
}
