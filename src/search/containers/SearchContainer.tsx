import { connect } from "react-redux";
import { Album } from "../../models/Album";
import { AppState } from "../../store";
import { searchAlbums } from "../../reducers/search";
import { SearchField } from "../../playlists/SearchField";
import { SearchResults } from "../SearchResults";
import { bindActionCreators } from "redux";

const withSearch = connect<
  {
    query: string;
    results: Album[];
  },
  {
    onSearch(query: string): void;
  }
>(
  (state: AppState) => ({
    query: state.search.query,
    results: state.search.results
  }),

  (dispatch: any) =>
    bindActionCreators(
      {
        onSearch: searchAlbums
      },
      dispatch
    )
  // (dispatch: any) => ({
  //   // onSearch: searchAlbums(dispatch)
  //   onSearch(query) {
  //     dispatch(searchAlbums(query));
  //   }
  // })
);

export const AlbumsSearch = withSearch(SearchField);
export const AlbumsResults = withSearch(SearchResults);
