import * as React from "react";
import { Album } from "../models/Album";
import './SearchResults.css'

interface IProps {
  results: Album[];
}

export class SearchResults extends React.Component<IProps> {
  render() {
    return (
      <div>
        <div className="card-group">
          {this.props.results.map(album => (

            <div className="card" key={album.id}>
              <img src={album.images[0].url} className="card-img-top" />

              <div className="card-body">
                <h3 className="card-title">{album.name}</h3>
              </div>
            </div>
         
         ))}
        </div>
      </div>
    );
  }
}
