import * as React from "react";
import { AlbumsSearch, AlbumsResults } from "./containers/SearchContainer";

export class SearchView extends React.Component<{}> {
  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <AlbumsSearch />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <AlbumsResults />
          </div>
        </div>
      </div>
    );
  }
}
