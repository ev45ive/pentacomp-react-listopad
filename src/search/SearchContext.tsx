import * as React from "react";
import { searchService } from "../services";

export const searchContext = React.createContext({
  query: "",
  results: [],
  search(query: string) {}
});

export class SearchProvider extends React.Component {
  state = {
    query: "",
    results: []
  };

  search = (query: string) => {
    if (!query) {
      return;
    }
    searchService.search(query).then(albums => {
      this.setState({
        query,
        results: albums
      });
    });
  };

  render() {
    return (
      <searchContext.Provider
        value={{
          ...this.state,
          search: this.search
        }}
      >
        {this.props.children}
      </searchContext.Provider>
    );
  }
}
