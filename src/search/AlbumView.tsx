import * as React from "react";
import { Album } from "../models/Album";
import { Track } from "../models/Track";

export interface IProps {
  album: Album;
  onLike(): void;
}

export const AlbumView = ({ album, onLike }: IProps) => {
  return (
    <div className="row">
      <div className="col">
        <div className="card">
          <h5 className="card-title album-title">{album.name}</h5>

          <button className="like-it" onClick={onLike}>
            Like it
          </button>
        </div>
      </div>
      <div className="col">
        {album.tracks && (
          <div className="track-list">
            {album.tracks.items.map(track => (
              <TrackItem track={track} key={track.id} />
            ))}
          </div>
        )}
      </div>
    </div>
  );
};

export const TrackItem = ({ track }: { track: Track }) => (
  <div className="track-item">{track.name}</div>
);
