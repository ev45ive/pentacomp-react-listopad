// https://reactjs.org/docs/test-renderer.html
// https://jestjs.io/docs/en/tutorial-react
// https://airbnb.io/enzyme/docs/guides/jest.html

import * as React from "react";
import { /* render, shallow, */ mount, ReactWrapper } from "enzyme";
import { IProps, TrackItem, AlbumView } from './AlbumView';
import { Album } from "../models/Album";

describe("Album View", () => {
  let wrapper: ReactWrapper<IProps, {}>;
  let album: Album;

  beforeEach(() => {
    album = {
      id: "123",
      name: "Placki",
      images: []
    };
    const like = jest.fn();
    wrapper = mount(<AlbumView album={album} onLike={like} />);
  });

  it("should create", () => {
    expect(wrapper.exists()).toBeTruthy();
  });

  it("should display title", () => {
    const title = wrapper.find(".album-title");

    expect(title.exists()).toBeTruthy();
    expect(title.text()).toMatch("Placki");
  });

  it("should display updated title", () => {
    const title = wrapper.find(".album-title");
    wrapper.setProps({
      album: { ...album, name: "Batman" }
    });
    expect(title.text()).toMatch("Batman");
  });

  it('shoud have "like-it" action', () => {
    const likeIt = wrapper.find(".like-it");
    expect(likeIt.exists()).toBeTruthy();
    likeIt.simulate("click");
    expect(wrapper.prop("onLike")).toHaveBeenCalled();
  });

  it("should render list of tracks if available", () => {
    expect(wrapper.find(".track-list").exists()).toBeFalsy();

    // wrapper.instance().magicMethod()

    wrapper.setProps({
      album: {
        ...album,
        tracks: {
          items: [
            {
              id: "123",
              name: "TestTrack 123"
            },
            {
              id: "234",
              name: "TestTrack 234"
            }
          ],
          total: 1
        }
      }
    });

    expect(wrapper.find(".track-list").exists()).toBeTruthy();
    const tracks = wrapper.find(TrackItem);
    expect(tracks.length).toBe(2);
    expect(tracks.at(0).text()).toMatch("TestTrack 123");
  });
});
