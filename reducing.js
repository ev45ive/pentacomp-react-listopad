inc = (x=1) => ({type:'INC', payload:x});
dec = (x=1) => ({type:'DEC', payload:x});
insert = (x={}) => ({type:'INSERT', payload:x});

counter = (state, action)=>{
	switch(action.type){
        case 'INC': return { ...state, counter: state.counter + action.payload };
        case 'DEC': return { ...state, counter: state.counter - action.payload };
        default: return state 
    }
};

playlist = (state, action)=>{
	switch(action.type){
        case 'INSERT': return { ...state, playlists: [...state.playlists, action.payload] };
        default: return state 
    }
};

[inc(),dec(), insert({name:'Placki'}),inc(2),inc()].reduce( (state, action)=>{
	state = counter(state,action)
	state = playlist(state,action)
	return state
},{
	counter: 0,
	playlists:[]
})